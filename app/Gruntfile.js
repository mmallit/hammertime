module.exports = function (grunt) {

	// Project configuration.
	grunt.initConfig({
		pkg: grunt.file.readJSON("package.json"),

		jshint: {
			options: {
				curly: true,
				eqeqeq: true,
				immed: true,
				latedef: true,
				newcap: true,
				noarg: true,
				sub: true,
				undef: true,
				boss: true,
				eqnull: true,
				browser: true,
				plusplus: false,
				smarttabs: true,
				evil: true,
				globals: {
					global: true,
					process: true,
					console: true,
					Buffer: true,
					require: true,
					__filename: true,
					__dirname: true,
					module: true,
					exports: true,
					//for Browser
					Backbone: true,
                    L : true,
					window: true,
					alert: true,
					$: true,
					define: true,
					requirejs: true,
					JsSHA: true
				}
			},
			files: {
				src: [
				      "Gruntfile.js",
				      "server.js",
				      "routes.js",
				      "config.json",
				      "libs/**/*.js",
				      "components/**/*.js",
				      "client/spa/**/*.js"]
			}
		},

		twbpkg: grunt.file.readJSON('client/styles/bootstrap-3.1.1/package.json'),
		banner: '/*!\n' +
			' * Bootstrap v<%= twbpkg.version %> (<%= twbpkg.homepage %>)\n' +
			' * Copyright 2011-<%= grunt.template.today("yyyy") %> <%= twbpkg.author %>\n' +
			' * Licensed under <%= twbpkg.license.type %> (<%= twbpkg.license.url %>)\n' +
			' */\n',
		jqueryCheck: 'if (typeof jQuery === \'undefined\') { throw new Error(\'Bootstrap\\\'s JavaScript requires jQuery\') }\n\n',

		// Clean the folders
		clean: {
			build: ["public/js/spa", "build", "public/css/vendor"],
			js: ["public/js/spa", "build"],
			css: ["public/css/vendor"]
		},

		// Copy all the .js files in spa and build
		copy: {
			spa: {
				files: [
					{
						expand: true,
						cwd: "client/spa/",
						src: ["**/*.js"],
						dest: "public/js/spa/"
                			},
					{
						expand: true,
						cwd: "build/",
						src: ["**/*.js"],
						dest: "public/js/spa/"
                			}
            			]
			}
		},

		// Concatonate bootstrap to single .js file
		concat: {
			options: {
				banner: '<%= banner %>\n<%= jqueryCheck %>',
				stripBanners: false
			},
			bootstrap: {
				src: [
					"client/styles/bootstrap-3.1.1/js/transition.js",
					"client/styles/bootstrap-3.1.1/js/alert.js",
					"client/styles/bootstrap-3.1.1/js/button.js",
					"client/styles/bootstrap-3.1.1/js/carousel.js",
					"client/styles/bootstrap-3.1.1/js/collapse.js",
					"client/styles/bootstrap-3.1.1/js/dropdown.js",
					"client/styles/bootstrap-3.1.1/js/modal.js",
					"client/styles/bootstrap-3.1.1/js/tooltip.js",
					"client/styles/bootstrap-3.1.1/js/popover.js",
					"client/styles/bootstrap-3.1.1/js/scrollspy.js",
					"client/styles/bootstrap-3.1.1/js/tab.js",
					"client/styles/bootstrap-3.1.1/js/affix.js"
            			],
				dest: "public/js/vendor/bootstrap.js"
			}
		},

		// Minify the bootstrap js
		uglify: {
			options: {
				report: 'min'
			},
			bootstrap: {
				options: {
					banner: '<%= banner %>'
				},
				src: 'public/js/vendor/bootstrap.js',
				dest: 'public/js/vendor/bootstrap.min.js'
			}
		},

		// Convert our html files to javaScript for backbone
		jst: {
			compile: {
				options: {
					prettify: true,
					processName: function (longPath) {
                        // This path needs to be the same size as 
                        // the path defined below inside the 'files:'
                        // block.
						return longPath.substr(21);
					}
				},
				files: {
					"build/templates.js": ["client/spa/templates/**/*.html"]
				}
			}
		},

		// Compiles all less files from bootstrap
		less: {
			compileCore: {
				options: {
					strictMath: true,
					sourceMap: true,
					outputSourceFiles: true,
					sourceMapURL: '<%= twbpkg.name %>.css.map',
					sourceMapFilename: 'client/styles/bootstrap-3.1.1/dist/css/<%= twbpkg.name %>.css.map'
				},
				files: {
					'client/styles/bootstrap-3.1.1/dist/css/<%= twbpkg.name %>.css': 'client/styles/bootstrap-3.1.1/less/bootstrap.less'
				}
			},
			compileTheme: {
				options: {
					strictMath: true,
					sourceMap: true,
					outputSourceFiles: true,
					sourceMapURL: '<%= twbpkg.name %>-theme.css.map',
					sourceMapFilename: 'client/styles/bootstrap-3.1.1/dist/css/<%= twbpkg.name %>-theme.css.map'
				},
				files: {
					'client/styles/bootstrap-3.1.1/dist/css/<%= twbpkg.name %>-theme.css': 'client/styles/bootstrap-3.1.1/less/theme.less'
				}
			},
			// Clean the css
			csscomb: {
				options: {
					config: 'client/styles/bootstrap-3.1.1/less/.csscomb.json'
				},
				dist: {
					files: {
						'client/styles/bootstrap-3.1.1/dist/css/<%= twbpkg.name %>.css': 'dist/css/<%= twbpkg.name %>.css',
						'client/styles/bootstrap-3.1.1/dist/css/<%= twbpkg.name %>-theme.css': 'dist/css/<%= twbpkg.name %>-theme.css'
					}
				}
			},
			// Minify the css
			minify: {
				options: {
					cleancss: true,
					report: 'min'
				},
				files: {
					'public/css/vendor/<%= twbpkg.name %>.min.css': 'client/styles/bootstrap-3.1.1/dist/css/<%= twbpkg.name %>.css',
					'public/css/vendor/<%= twbpkg.name %>-theme.min.css': 'client/styles/bootstrap-3.1.1/dist/css/<%= twbpkg.name %>-theme.css'
				}
			}
		},

		// Watch for any changes in .less files
		watch: {
			less: {
				files: ["client/styles/bootstrap/less/*.less"],
				tasks: ['less']
			}
		}

	});

	grunt.loadNpmTasks("grunt-contrib-jshint");
	grunt.loadNpmTasks("grunt-contrib-concat");
	grunt.loadNpmTasks("grunt-contrib-less");
	grunt.loadNpmTasks("grunt-contrib-cssmin");
	grunt.loadNpmTasks("grunt-contrib-uglify");
	grunt.loadNpmTasks("grunt-contrib-jst");
	grunt.loadNpmTasks("grunt-contrib-watch");
	grunt.loadNpmTasks("grunt-csscomb");
	grunt.loadNpmTasks("grunt-contrib-copy");
	grunt.loadNpmTasks('grunt-contrib-clean');

	// Default task(s).
	grunt.registerTask("default", ["clean:build", "jshint", "jst", "copy:spa", "less", "concat", "uglify"]);
	grunt.registerTask("css", ["clean:css", "less"]);
	grunt.registerTask("js", ["clean:js", "jshint", "jst", "copy"]);
	grunt.registerTask("ui", ["css", "js"]);
	grunt.registerTask("bootstrap", ["concat", "uglify"]);
};
