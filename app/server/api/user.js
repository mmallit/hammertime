var express = require("express"),
	users = express.Router();

// --------Begin private fields block--------
//
var userService;
//
// ---------End private fields block---------

// --------Begin API Initialization--------
//
exports.init = function (userservice) {
	userService = userservice;
};
//
// ---------End API Initialization---------

// --------Begin Routes--------
//

// --------Begin /user/franchises/--------

///
// DO NOT MOVE THIS. Order matters in api. If a request has no parameters it MUST 
// go at the top of the file, otherwise the routing will see the route as a parameter
// and try to execute against the first parameterized api method
///
users.get("/franchises", function (req, res, next) {
	var result = userService.getAllFranchises(function (error, msg) {
		if (msg) {
			res.json(msg);
		} else {
			res.send(500);
		}
	});
});
// ---------End /user/franchises/---------

// --------Begin /user/add--------
users.post("/add", function (req, res, next) {
	var user = req.body;

	var result = userService.addUser(req.host, user, function (error, success) {
		if (success) {
			res.json("success");
		} else {
			res.send(500);
		}
	});
});
// ---------End /user/add---------

// --------Begin /user/:sessionToken--------
users.get("/:token", function (req, res, next) {
	var token = req.params.token;
	// Gets the account object from the token
	var result = userService.getByToken(token, function (error, user) {
		if (user) {
			res.json(user);
		} else {
			res.send(500);
		}
	});
});
// ---------End /user/register/key---------

// --------Begin /user/endSession/:sessionToken--------
users.get("/endSession/:id", function (req, res, next) {
	var id = req.params.id;

	var result = userService.endSession(id, function (error, msg) {
		if (msg) {
			res.json(msg);
		} else {
			res.send(500);
		}
	});
});
// ---------End /user/endSession/:sessionToken---------

exports.routes = users;