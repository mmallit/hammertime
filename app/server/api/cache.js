var express = require("express");

var cache = express.Router();

// --------Begin private fields block--------
//
var cacheService;
//
// ---------End private fields block---------

// --------Begin API Initialization--------
//
exports.init = function (cacheservice) {
	cacheService = cacheservice;
};
//
// ---------End API Initialization---------

// --------Begin Routes--------
//
// --------Begin /user/login/user/pass--------
var get = function (req, res, next) {
    var cacheKey = req.originalUrl.split("?")[0];
	//var id = req.url;
	cacheService.get(cacheKey, function (error, result) {
		if (result && !error) {
            //console.log(JSON.parse(result));
            console.log(result);
			res.json(result);
		} else {
			next();
		}
	});
};

// ---------End /user/login/user/pass---------

exports.get = get;