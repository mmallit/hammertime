var express = require("express");
var auth = express.Router();

// --------Begin private fields block--------
//
var authService;
var cacheService;
//
// ---------End private fields block---------

// --------Begin API Initialization--------
//
exports.init = function (authservice, cacheservice) {
	authService = authservice;
	cacheService = cacheservice;
};
//
// ---------End API Initialization---------

// --------Begin Routes--------
//

// --------Begin auth/activate/key--------
auth.get("/activate/:key", function (req, res, next) {
	var key = req.params.key;
    console.log("Activating user");

	var result = authService.activateUser(key, function (error, user) {
		var helpfulMsg = 'Error activating user';
		if(error){
			console.log(helpfulMsg + '\n' + error);
			res.send(401);
			return;
		}
		if (!user) {
			console.log(helpfulMsg + ', undefined user');
			res.send(401);
			return;
		}
		//console.log(JSON.stringify(user));
        console.log(user);
		res.json(user.account_id);
	});
});
// ---------End /auth/activate/key---------

// --------Begin /auth/setPassword--------
auth.post("/setPassword", function (req, res, next) {
	var userId = req.body.userId;
	var password = req.body.password;

	var result = authService.setPassword(userId, password, function (error, user) {
		if (user) {
            console.log(user);
			res.json({
				email: user
			});
		} else {
			res.send(500);
		}
	});
});
// ---------End /auth/setPassword---------

// --------Begin /auth/login--------
auth.post("/login", function (req, res, next) {
	var user = req.body.username;
	var password = req.body.password;

	var result = authService.login(user, password, function (error, user) {
		if (user) {
			// add the user to the cache using the token
			cacheService.add(user.token, user);
			res.json(user.token);
		} else {
			res.send(401);
		}
	});
});
// ---------End /auth/login---------

// --------Begin get authorization--------
var get = function (req, res, next) {
	var token = req.query.token;
	cacheService.get(token, function (error, result) {
		if (result) {
			req.account = result;
			next();
		} else {
			console.log("User token failed authentication");
			res.send(401);
		}
	});
};
// ---------End get authorization---------

exports.routes = auth;
exports.get = get;
