var seq = require('seq');

var user = require("./user.js"),
	cache = require("./cache.js"),
	auth = require("./auth.js");

var dbs = require("./../services/db.js"),
	authservice = require("./../services/auth.js"),
	userservice = require("./../services/user.js"),
	cacheservice = require("./../services/cache.js"),
	userAggregate = require("./../aggregateroots/user.js");

exports.setup = function () {
	// --------Begin DB Initialization--------
	//
	var db = dbs.init('postgres://postgres:pass@word1@localhost:5432/hammertime');
	//
	// ---------End DB Initialization---------

	// --------Begin Aggregate Initialiazation---------
	//
	userAggregate.init(db);
	//
	// ---------End Aggregate Initialization-----------

	// --------Begin Service Initialization--------
	//
	authservice.init(db, userAggregate);
	userservice.init(db, userAggregate);
	cacheservice.init();
	//
	// ---------End Service Initialization---------

	// --------Begin API Initialization--------
	//
	auth.init(authservice, cacheservice);
	cache.init(cacheservice);
	user.init(userservice);
	//
	// ---------End API Initialization---------

};

// publicly expose the API regions
exports.auth = auth;
exports.user = user;
exports.cache = cache;