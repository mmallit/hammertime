// --------Begin Private Fields--------
//
var db;
//
// ---------End Private Fields---------

// --------Begin Service Initialization--------
//
var init = function (dbservice) {
	this.db = dbservice;
};
//
// ---------End Service Initialization---------


// --------Begin Aggregate Root Methods--------
//

///
// Retrieves a user object by their session token
///
var getBySessionToken = function (token, cb) {
	var thisToken = token;
	var userScript =
		"SELECT account.account_id, account.firstname, account.lastname, account.displayname, account.token, account.email, account.created_on, account.last_login, account_role.grant_date, role.role_id, role.role_name, registration.registration_id, registration.key, registration.is_registered " +
		"FROM account, account_role, role, registration " +
		"WHERE account.account_id = account_role.account_id " +
		"AND account_role.role_id = role.role_id " +
		"AND account.account_id = registration.account_id " +
		"AND account.token = $1;";

	this.db.first(userScript, [token], function (err, user) {
		if (err) {
			console.log("Error retrieving user by token from db: " + err);
			cb(err, null);
		} else if (user) {
			console.log("Retrieved user " + user.displayname + " by token from db");
			cb(null, user);
		} else {
			var msg = "No user returned by token from db";
			console.log(msg);
			cb(msg, null);
		}
	});
};

var getByUsernameAndPassword = function (username, password, cb) {
	console.log("Attempting to retrieve user " + username);
	var userScript =
		"SELECT account.account_id, account.firstname, account.lastname, account.displayname, account.token, account.email, account.created_on, account.last_login, account_role.grant_date, role.role_id, role.role_name, registration.registration_id, registration.key, registration.is_registered " +
		"FROM account, account_role, role, registration " +
		"WHERE account.account_id = account_role.account_id " +
		"AND account_role.role_id = role.role_id " +
		"AND account.account_id = registration.account_id " +
		"AND account.email = $1 " +
		"AND account.password = $2;";

	console.log(userScript);

	this.db.first(userScript, [username, password], function (err, user) {
		var helpfulMsg = 'Error retrieving user with credentials';
		if (err) {
			console.log(helpfulMsg + '\n' + err);
			cb(err, null);
			return;
		}
		if(!user){
			console.log(helpfulMsg + ', user is undefined');
			cb(helpfulMsg, null);
			return;
		}
		console.log("Retrieved user " + user.displayname + " by credentials");
		cb(null, user);
	});
};


//
// ---------End Aggregate Root Methods---------

exports.init = init;
exports.getBySessionToken = getBySessionToken;
exports.getByUsernameAndPassword = getByUsernameAndPassword;
