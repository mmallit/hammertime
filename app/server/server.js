var config = require("./../config.json"),
	api = require("./api/api.js"),
	express = require("express"),
	morgan = require("morgan"),
	bodyParser = require("body-parser"),
	favicon = require("serve-favicon"),
	app = express();

// --------Begin configuration--------
//
// Read the configuration and set the ports and public directory
config.server.port = process.env.PORT || config.server.port;
config.server.public_dir = process.env.PUBLIC_DIR || config.server.public_dir;

// setup the logger for express 4
// this was previously app.use(express.logger('dev'));
app.use(morgan({
	format: 'dev',
	immediate: true
}));

// setup for the favicon to point to specific .ico
app.use(favicon(__dirname + '/../public/favicon.ico'));

// --------Performance Note--------
// #1 configure routes
// #2 configure static content
// otherwise the disk will be hit for every API call
// http://stackoverflow.com/questions/12695591/node-js-express-js-how-does-app-router-work

// --------Begin Body Parser--------
// https://www.npmjs.org/package/body-parser
// parse application/json and application/x-www-form-urlencoded
app.use(bodyParser())
// parse application/vnd.api+json as json
app.use(bodyParser.json({
	type: 'application/vnd.api+json'
}))
// ---------End Body Parser---------

// --------Begin route configuration--------
console.log("server.js: loading routes");
// load the routes by region
// see Node.js In Action page 141
app.use("/api", api.auth.routes);
app.use("/api", api.auth.get);
app.use("/api", api.cache.get);

app.use("/api/user", api.user.routes);

// ---------End route configuration---------

// --------Begin front end configuration--------
// gzip compression for static content 
// use in production
// app.use(express.compress());
//
// serves front-end to client (static content)
// static is configured first, our routes second (see EOF),
// so on any conflicts, static will win. This means
// any pages that have the same name as an API call will
// be served instead of the API call.	
app.use(express["static"](__dirname + config.server.public_dir));
// ---------End front end configuration---------

// dev statement, remove for prod
console.log("server.js: server configuration finished");

// specify the configuration for a conditional environment.
var env = process.env.NODE_ENV || 'development';
if ('development' == env) {
	// configure stuff here
}
//
// ---------End configuration---------

// --------Begin API Setup--------
//
// setup of the api to create repositories and providers
api.setup();
//
// ---------End API Setup---------

// --------Begin listen--------
//
// Binds express to node.js' listen function
// Starts accepting connections on the given port
// http://expressjs.com/4x/api.html#app.listen
console.log("server.js: attaching to port " + config.server.port);
app.listen(config.server.port);
console.log("server.js: App listening on port: " + config.server.port);
//
// ---------End listen---------