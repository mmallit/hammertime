var nodemailer = require("nodemailer");
var config = require("./../../config.json");
var uuid = require('node-uuid');
// --------Begin Private Fields--------
//
var db;
var aggregate;
//
// ---------End Private Fields---------

// --------Begin Service Initialization--------
//
var init = function (dbService, userAggregate) {
    db = dbService;
    aggregate = userAggregate;
};
//
// ---------End Service Initialization---------


// --------Begin Service Methods--------
//

///
// Returns out a token for the user logging in
///
var login = function (username, password, cb) {
    aggregate.getByUsernameAndPassword(username, password, function (err, user) {
        // Logging done in aggregate for get by username and password
        if (err) {
            cb(err, null);
        } else {
            if (user.account_id) {
                // Log here 
                console.log("Creating token for " + user.displayname);
                createSessionToken(user.account_id,
                    function (err, token) {
                        if (token) {
                            user.token = token;
                            console.log("Token successfully created for " + user.displayname);
                            cb(null, user);
                        }
                    });

            } else {
                var msg = "Unable to retrieve " + username + "'s Id";
                console.log(msg);
                cb(msg, null);
            }
        }
    });
}

///
// Creates a session token for the given user by id
///
var createSessionToken = function (id, cb) {
    var token = uuid.v1();
    var createTokenScript = "UPDATE account SET token=$1 WHERE account_id=$2;";

    db.query(createTokenScript, [token, id], function (err, row) {
        if (err) {
            console.log("Failed to create token for user with id: " + id + "\n" + err);
            cb(err, null);
        } else {
            cb(null, token);
        }
    })
};

///
// Activates a user when their registration key is presented
///
var activateUser = function (key, cb) {
    // Set registration recored to registered
    var isreg = "SELECT count(account_id) FROM registration WHERE is_registered=TRUE and key=$1;";
    var updreg = "UPDATE registration SET is_registered=TRUE WHERE key=$1;";
    var selreg = "SELECT account_id FROM registration WHERE key=$1;";
    db.query(isreg, [key], function (err, irow) {
        if (err) {
            cb(err, null);
            return;
        }
        if (irow && irow.length > 0 && irow[0].count > 0) {
            cb("This user has already been registered", null);
            return;
        }
        db.query(updreg, [key], function (err, urow) {
            if (err) {
                cb(err, null);
                return;
            }
            if (!urow) {
                cb('No record returned updating user registration', null);
                return;
            }
            if (urow.length === 0) {
                cb('Registration is does not exist', null);
                return;
            }

            db.first(selreg, [key], function (err, srow) {
                if (err) {
                    cb(err, null);
                    return;
                }
                cb(null, srow);
            });
        });
    });
};

///
// Sets the password for a given user by id
///
var setPassword = function (userid, password, cb) {
    // Save password for user
    // { password, userid }
    var updatepasswordscript = "UPDATE account SET password=$1 WHERE account_id=$2;";
    var selectuserscript = "SELECT email FROM account WHERE account_id=$1;";

    db.query(updatepasswordscript, [password, userid], function (err, row) {
        if (err) {
            cb(err, null);
        } else {
            db.first(selectuserscript, [userid], function (err, user) {
                cb(null, user.email);
            });
        }
    });
};
//
// ---------End Service Methods---------

exports.login = login;
exports.activateUser = activateUser;
exports.setPassword = setPassword;
exports.init = init;