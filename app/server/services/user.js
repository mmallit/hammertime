var nodemailer = require("nodemailer");
var config = require("./../../config.json");
var uuid = require('node-uuid');
// --------Begin Private Fields--------
//
var db;
var aggregate;
//
// ---------End Private Fields---------

// --------Begin Service Initialization--------
//
var init = function (dbService, userAggregate) {
    db = dbService;
    aggregate = userAggregate;
};
//
// ---------End Service Initialization---------


// --------Begin Service Methods--------
//
var sendEmail = function (host, email, user, key) {
    // Create a Sendmail transport object
    var smtpTransport = nodemailer.createTransport("SMTP", {
        service: "Gmail",
        auth: {
            user: "SportsBIHelp@gmail.com",
            pass: "MikeGrahamShaun"
        }
    });
    var url = host + ":" + config.server.port;
    // setup e-mail data with unicode symbols
    var mailOptions = {
        from: "SportsBI Help <SportsBIHelp@gmail.com>", // sender address
        to: email, // list of receivers
        subject: "Welcome to Sports BI " + user, // Subject line
        text: "Welcome to Sports BI " + user + ".\n\nHere is you key: " + key+"\n\nPlease go to http://" + url, // plaintext body
        html: "<p><b>Welcome to Sports BI " + user + "!</p><p>Here is your key: " + key + "</p></b><b><p>Please go to <a href=http://" + url + ">http://" + url + "</a> to sign up.</b></p>" // html body
    }

    // send mail with defined transport object
    smtpTransport.sendMail(mailOptions, function (error, response) {
        if (error) {
            console.log(error);
        } else {
            console.log("Message sent: " + response);
        }
    });

};

///
// Retrieves a user object by their session token
///
var getByToken = function (token, cb) {
    aggregate.getBySessionToken(token, function (err, user) {
        // Logging done at aggregate level
        if (err) {
            cb(err, null);
        } else {
            cb(null, user);
        }
    })
}

///
// Removes a session token for the given user by id
///
/// !----- TODO: cacheService.remove(token); ------
var endSession = function (id, cb) {
    var token = null;
    var createTokenScript = "UPDATE account SET token=$1 WHERE account_id=$2;";

    db.query(createTokenScript, [token, id], function (err, row) {
        if (err) {
            console.log("Failed to remove token for user with id: " + id + "\n" + err);
            cb(err, null);
        } else {
            console.log("Removed token for user with id: " + id);
            cb(null, "success");
        }
    })
};

var getAllFranchises = function (cb) {
    var franchisesScript =
        "SELECT * " +
        "FROM franchise " +
        "ORDER BY name;";

    db.query(franchisesScript, function (err, row) {
        if (err) {
            console.log("Failed to retrieve franchises");
            cb(err, null);
        } else {
            cb(null, row);
        }
    })
};

///
// Adds a user to the db and creates a registration key for them
///
var addUser = function (host, user, cb) {
    var email = user.email;
    var name = user.name;
    var franchise = user.franchise;
    var userId;

    // Generate key for the user to use in an email
    var key = uuid.v1();
    // Logic to add user to user table and to registration table
    // (firstName, lastName, displayName, password, email)
    var insertaccountscript = "INSERT INTO account(firstname, lastname, displayname, password, email, franchise, created_on) VALUES ($1, $2, $3, $4, $5, $6, current_date);";
    var selectaccountscript = "SELECT account_id FROM account WHERE email=$1;";
    // userId, key
    var insertregistrationscript = "INSERT INTO registration(account_id, key, is_registered) VALUES ($1, $2, false)";
    var insertrolescript = "INSERT INTO account_role(account_id, role_id, grant_date) VALUES ($1, 2, current_date);"

    // Perform the query against the db
    db.query(insertaccountscript, [user.name, '', user.name, 'pass@word1', user.email, franchise], function (err, row) {
        if (err) {
            cb(err, null);
        } else {
            db.first(selectaccountscript, [email], function (err, userrow) {
                console.log(userrow);
                userId = userrow.account_id;
                // If we got a valid userId
                if (userId) {
                    // log that we have added the user
                    console.log(user.name + " has been added as a user with");
                    // Insert the registration info into the db
                    db.query(insertrolescript, [userId], function (err, empty) {
                        if (err) {
                            cb("Failed to add account_role: " + user.name + "\n" + err, null);
                        } else {
                            db.query(insertregistrationscript, [userId, key], function (err, row) {
                                if (err) {
                                    cb("Failed to add user: " + user.name + "\n" + err, null);
                                } else {
                                    // Email out the registration with key
                                    sendEmail(host, user.email, user.name, key);
                                    cb(null, "success");
                                }
                            });
                        }
                    });
                } else {
                    cb("Failed to add user: " + user.name + "\nUnable to retriev the user's Id", null);
                }
            });
        }
    });
};

//
// ---------End Service Methods---------

exports.getAllFranchises = getAllFranchises;
exports.getByToken = getByToken;
exports.endSession = endSession;
exports.addUser = addUser;
exports.init = init;