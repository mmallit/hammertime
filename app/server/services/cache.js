var redis = require('redis'),
	url = require('url');

// --------Begin Private Fields--------
//
var redisClient;
//
// ---------End Private Fields---------

// --------Begin Service Initialization--------
//
var init = function () {
	if (process.env.REDISCLOUD_URL) {
		// https://devcenter.heroku.com/articles/rediscloud#using-redis-from-node-js
		console.log('Redis: heroku cloud');
		console.log('Startup (automatic): provisioned via heroku');
		console.log('Command (documentation): heroku addons:docs rediscloud');

		var redisURL = url.parse(process.env.REDISCLOUD_URL);
		redisClient = redis.createClient(redisURL.port, redisURL.hostname, {
			no_ready_check: true
		});
		redisClient.auth(redisURL.auth.split(":")[1]);
	} else {
		// http://redis.io
		console.log('Redis: local');
		console.log('Startup: terminal');
		console.log('Command: ./redis-server');
		redisClient = redis.createClient();
	}
	redisClient.on("error", function (err) {
		console.log("Error: " + err);
	});
};
//
// ---------End Service Initialization---------


// --------Begin Service Methods--------
//
var add = function (id, item) {
	redisClient.set(id, JSON.stringify(item), redis.print);
};

// cb(err, result)
var get = function (id, cb) {
	redisClient.get(id, function (err, result) {
		if (err) {
			console.log("Error retrieving item from cache: " + err);
			cb(err, null);
			return;
		}

		if (!result) {
			cb(null, null);
			return;
		}

		cb(null, JSON.parse(result));
		return;
	});
};

// cb(err, result)
var exists = function (id, cb) {
	redisClient.get(id, function (err, result) {
		if (err) {
			cb(err, null);
			return;
		}

		cb(null, result);
	});
};

//
// ---------End Service Methods---------

exports.add = add;
exports.get = get;
exports.init = init;
exports.exists = exists;