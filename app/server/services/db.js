// Mercilessly stolen from pg-query
var pg;
// Use whichever pg module is available 
try {
	pg = require('pg');
} catch (e) {
	try {
		pg = require('pg.js');
	} catch (e) {
		throw new Error("Could not require pg or pg.js - please install one or the other");
	}
}

var util = require('util');

var Query = function () {
	this.name = null;
	this.text = null;
	this.values = null;
};

// --------Begin Service Initialization--------
//
var init = function (path) {
	// Have to set the path on an object otherwise it is lost 
	// **** need to understand why this is happening ****
	pg.conn = path;
	console.log("db.js: initialized");

	return this;
};

// Returns cb(error, rows, result)
var query = function (text, values, cb) {
	if (!text) {
		cb("No text found to query", null);
	} else if (typeof text !== 'string') {
		cb("Text must be a string", null);
	} else {
		if (typeof values == 'function') {
			cb = values;
			values = [];
		}

		// Create a new query object
		var q = new Query();
		q.text = text;
		q.values = values;
		
		pg.connect(pg.conn, function (err, client, done) {
			if (err) {
				console.log("pg.connect " + err);
				cb(err, null, null);
			} else {
				client.query(q, function (err, result) {
					if (err) {
						done();
						console.log("client.query " + err);
						cb(err, null, null);
					} else {
						done();
						cb(null, result.rows);
					}
				});
			}
		});
	}
};

// returns cb(error, row, result);
var first = function (text, values, cb) {
	if (typeof values == 'function') {
		cb = values;
		values = [];
	}
	if (values && !util.isArray(values)) {
		values = [values];
	}
	query(text, values, function (err, rows) {
		return cb(err, rows ? rows[0] : null);
	});
};

//
// ---------End Service Initialization---------

exports.init = init;
exports.query = query;
exports.first = first;