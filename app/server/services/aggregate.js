// Aggregate functions:
// https://en.wikipedia.org/wiki/Aggregate_function
//
// Looping in JavaScript:
// https://blogs.oracle.com/greimer/entry/best_way_to_code_a
//
// Array contains:
// http://stackoverflow.com/questions/237104/array-containsobj-in-javascript
//
// Max / Min
// http://ejohn.org/blog/fast-javascript-maxmin/
// 
// Median
// http://caseyjustus.com/finding-the-median-of-an-array-with-javascript
//

var util = require('util');

module.exports = function (operation, values, cb) {
    if (!operation) {
        cb("aggregate.js - An aggregation operation must be supplied", null);
        return;
    }

    if (typeof operation !== 'string') {
        cb("aggregate.js - The aggregation operation must be a text value", null);
        return;
    }

    //Average()
    //Count()
    //Maximum()
    //Median()
    //Minimum()
    //Mode()
    //Sum()

    var ops = ['average', 'count', 'maximum', 'median', 'minimum', 'mode', 'sum'];

    if (!contains(ops, operation)) {
        var err = "aggregate.js - The aggregation operation " +
            "must be one of the following: ";
        for (var i = 0; i <= ops.length; i++) {
            err += ops[i];
            if (i != ops.length - 1) {
                err += ', ';
            }
        }
        cb(err, null);
        return;
    }

    if (!values) {
        cb("aggregate.js - Values must be supplied", null);
        return;
    }

    if (!util.isArray(values)) {
        values = [values];
    } else if (values.lengthi == 0 || !values[0]) {
        cb("aggregate.js - Values must be supplied", null);
    }

    switch (operation) {
    case 'sum':
        var s = sum(values);
        cb(null, s);
        return s;
    case 'average':
        var a = avg(values);
        cb(null, a);
        return a;
    case 'count':
        var c = count(values);
        cb(null, c);
        return c;
    case 'maximum':
        var m = max(values);
        cb(null, m);
        return m;
    case 'minimum':
        var m = min(values);
        cb(null, m);
        return m;
    case 'median':
        var m = median(values);
        cb(null, m);
        return m;
    }
};

var avg = function (a) {
    average = sum(a) / a.length;
    return average;
};

var count = function (a) {
    return a.length;
};

var max = function (a) {
    return Math.max.apply(Math, array);
};

var median = function (a) {
    a.sort(function (a, b) {
        return a - b;
    });

    var half = Math.floor(a.length / 2);

    if (values.length % 2)
        return values[half];
    else
        return (values[half - 1] + values[half]) / 2.0;
};

var min = function (a) {
    return Math.min.apply(Math, array);
};

var mode = function (a) {

};

var sum = function(a) {
    var sum = 0;
    var i = a.length;
    while (i--) {
        sum += parseFloat(a[i]);
    }
    return sum;
};

var contains = function (a, obj) {
    var i = a.length;
    while (i--) {
        if (a[i] === obj) {
            return true;
        }
    }
    return false;
}