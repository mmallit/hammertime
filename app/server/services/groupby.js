var util = require('util');

module.exports = function (cols, rows, cb) {
    if (!cols) {
        cb("groupby.js - You must provide at least one col", null);
        return;
    }

    if (!rows || !rows.length || !rows[0]) {
        cb("groupby.js - You must provide at least one row", null);
        return;
    }

    if (cols && !util.isArray(cols)) {
        cols = [cols];
    }

    // Loop through the rows,
    // then group through the roles
    var group = {};
    for (var i = rows.length; --i >= 0;) {
        var row = rows[i];

        var subgroup = group;
        for (var j = 0; j < cols.length; j++) {
            var col = cols[j];
            var val = row[col];

            // Determine if this is the last group
            var lowestGrain = j == cols.length - 1;

            // If this isn't the last group
            if (!lowestGrain) {
                // If we don't have a
                // group for this value
                if (!subgroup[val]) {
                    // Create a group
                    subgroup[val] = {};
                }
                // step down to the next group	
                // then on through the next loop 
                subgroup = subgroup[val];
            }

            // If this is the last group
            if (lowestGrain) {
                // If we don't have an
                // array for this value
                if (!subgroup[val]) {
                    // Create an array
                    subgroup[val] = [];
                }

                // Push the row onto
                // the array matching it's
                // dimensionality coordinates	
                subgroup[val].push(row);
            }
        }
    }

    return group;
}