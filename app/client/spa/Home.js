define([
 "spa/templates"
 ],
    function (templates) {
        var Home = Backbone.Marionette.ItemView.extend({
            initialize: function () {
                this.app = this.options.app;
            },
            template: function () {
                return window.JST["home.html"];
            },
            events: {
				"click #tasks-btn": "showTasks",
				"click #workouts-btn": "showWorkouts",
				"click #alerts-btn": "showAlerts",
            },
            ui: {
                tasksBtn: "#tasks-btn",
                workoutsBtn: "#workouts-btn",
                alertsBtn: "#alerts-btn"
            },
            onRender: function () {
                // Don't need to do anything yet...
            },
            showTasks: function(){
                this.app.showTasks();
                //alert("Tasks");
            },
            showWorkouts: function(){
                alert("Workouts");
            },
            showAlerts: function(){
                alert("Alerts");
            }
        });

        return Home;
    });