define([
	"spa/templates"
	],
	function (templates) {
		var SignIn = Backbone.Marionette.ItemView.extend({
			initialize: function () {
				this.app = this.options.app;
			},
			template: function () {
				return window.JST["signIn.html"];
			},
			events: {
				"click #signin-btn": "signIn",
				"click #first-time-link": "showRegister"
			},
			ui: {
				username: "#usernameEntry",
				password: "#passwordEntry",
				key: "#keyEntry",
				firstTimeLink: "#first-time-link",
				signinBtn: "#signin-btn",
				newPassword: "#newPasswordEntry",
				confirmPassword: "#confirmPasswordEntry"
			},
			onRender: function () {
				// Don't need to do anything yet...
			},
			signIn: function (e) {
				var login = $(this.ui.signinBtn).data("loginType");

				switch (login) {
				case "login":
					this.login(e);
					break;
				case "activate":
					this.activate();
					e.preventDefault();
					break;
				case "save":
					this.setPassword();
					e.preventDefault();
					break;
				}
			},
			showRegister: function (e) {
				// Show the key entry
				$(this.ui.key).show();
				// Hide the username 
				// password and link
				$(this.ui.username).hide();
				$(this.ui.password).hide();
				$(this.ui.firstTimeLink).hide();

				// Set the signinBtn's data to 'activate' 
				$(this.ui.signinBtn).data("loginType", "activate");

				// Change the btn text to 'Activate'
				$(this.ui.signinBtn).html("Activate");
				// prevent the reloading of the page
				e.preventDefault();
			},
			activate: function () {
				// Get the key value
				var key = $(this.ui.key).val();
				// Give access in inner scope
				var that = this;

				// If there was an error
				var error = function (err) {
					alert("Invalid Key\n" + err);
				};
				// If we get a successful response
				var success = function (id) {
					that.showSetPassword();
					that.userId = id;
				};
				// Make the register call
				this.app.activate(
					key,
					error,
					success
				);
			},
			showSetPassword: function () {
				// Hide the key input
				$(this.ui.key).hide();
				// Show the new password entry boxes
				$(this.ui.newPassword).show();
				$(this.ui.confirmPassword).show();
				// Set the data-login-type to save and change the display text to 'Save'
				$(this.ui.signinBtn).data("loginType", "save");
				$(this.ui.signinBtn).html("Save");
			},
			setPassword: function () {
				// allow access to this in inner scope
				var that = this;
				// Get the values from the password inputs
				var newPass = $(this.ui.newPassword).val();
				var confirmPass = $(this.ui.confirmPassword).val();
				// If there was an error show it
				var error = function (err) {
					alert(err);
				};
				// If successful show the login screen
				var success = function (email) {
					that.email = email.email;
					that.showLogin();
					alert("Success! \nSign in with your new password.");
				};
				// If the passwords match, make the setPassword call 
				if (newPass === confirmPass && this.userId) {
					this.app.setPassword(
						this.userId,
						newPass,
						error,
						success
					);
				} else {
					alert("The passwords must match in order to continue.");
				}
			},
			showLogin: function () {
				// Hide the new password entry boxes
				$(this.ui.newPassword).hide();
				$(this.ui.confirmPassword).hide();
				// Show the username and password entry boxes
				$(this.ui.username).show();
				$(this.ui.password).show();
				// If we have an email from set password, set the username
				if (this.email) {
					$(this.ui.username).val(this.email);
				}
				// Set the data-login-type to 'login' and change the display text to 'Sing In'
				$(this.ui.signinBtn).data("loginType", "login");
				$(this.ui.signinBtn).html("Sign In");
			},
			login: function (e) {
				// allow access in inner scope
				var that = this;
				// get the username and password values
				var username = this.$(this.ui.username).val();
				var password = this.$(this.ui.password).val();

				// are the username/password valid
				var isValid =
					$(this.ui.username)[0].checkValidity() &&
					$(this.ui.password)[0].checkValidity();

				// if not return
				if (!isValid) {
					return;
				}

				// If there was an error display it
				var error = function (error) {
					e.preventDefault();
					alert(error);
				};
				// If successful show the main content
				var success = function (token) {
					// store the session token in the cookies
					$.cookie('token', token, {
						expires: 30
					});
					that.app.token = token;

					// get the user object by the token
					that.app.getUserByToken(
						token,
						function (err) {
							alert(err);
						},
						function (user) {
							// if the user is registered
							if (user.is_registered === true) {
								// prevent the click event from doing anything else
								e.preventDefault();
								// show the main content passing in the user
								that.options.app.showMainContent(user);
							} else {
								// show the registration screen
								that.showRegister(e);
							}
						});
				};
				// Make the login call
				this.app.login(
					username,
					password,
					error,
					success
				);
			}
		});

		return SignIn;
	});