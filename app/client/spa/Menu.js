define([
	"spa/templates"
	],
	function (templates) {
		var Menu = Backbone.Marionette.ItemView.extend({
			initialize: function () {
				this.app = this.options.app;
				this.user = this.options.user;
			},
			template: function () {
				return window.JST["menu.html"];
			},
			events: {
				"click #logout-link": "logout",
				"click #save-btn": "addUser",
				"click #add-user-link": "clearData"
			},
			ui: {
				media: ".media",
				profileImage: "#profile-image",
				profileName: "#profile-name",
				dropdown: "#dropdown-menu",
				inputName: "#input-name",
				inputEmail: "#input-email",
				modal: "#modal",
				container: "#container"
			},
			onRender: function () {
				this.showAddUser(this.options.user.role_name);
				this.showUsername(this.options.user.displayname);
			},
			clearData: function () {
				// Reset the values for name, email and franchiseOptions
				$(this.ui.inputName).val("");
				$(this.ui.inputEmail).val("");
			},
			addUser: function (e) {
				// Access to this in inner scope
				var that = this;
				// Check the validity of the email and name and assigns a bool value
				var validEmail = $(this.ui.inputEmail)[0].checkValidity();
				var validName = $(this.ui.inputName)[0].checkValidity();
				// If the name and email are valid and not in the middle of saving
				if (validEmail && validName && !this.isSaving) {
					// We are saving now...
					this.isSaving = true;
					// get the values from the form inputs
					var email = $(this.ui.inputEmail).val();
					var name = $(this.ui.inputName).val();

					// If an error occurs
					var error = function (err) {
						e.preventDefault();
						that.isSaving = false;
						alert(err);
					};
					// If successful
					var success = function () {
						that.isSaving = false;
						$(that.ui.modal).modal('hide');
					};
					// Make the add user call
					this.app.addUser(
						email,
						name,
						error,
						success);
				} else {
					var msg = "To save you must provide:\n";
					if (!validEmail) {
						msg += " - A valid e-mail\n";
					}
					if (!validName) {
						msg += " - A name";
					}
					alert(msg);
				}
			},
			showAddUser: function (role) {
				if (role === "administrator") {
					var addBtn = "<!--Add User Link--><li><a href='#'' data-toggle='modal' data-target='#modal' id='add-user-link'>Add User</a></li>";
					this.ui.container.prepend(addBtn);
				}
			},
			showUsername: function (username) {
				if (username) {
					this.ui.profileName.text("Hello " + username);
					$(this.ui.media).show();
				}
			},
			logout: function () {
				$.removeCookie('token');

				var that = this;
				this.app.endSession(
					this.user.account_id,
					function () {
						alert("Failed to remove session token from database");
					},
					function () {
						that.app.showLogin();
					});
			}
		});
		return Menu;
	});