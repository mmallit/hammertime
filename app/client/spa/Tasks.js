define([
 "spa/templates"
 ],
    function (templates) {
        var Tasks = Backbone.Marionette.ItemView.extend({
            initialize: function () {
                this.app = this.options.app;
            },
            template: function () {
                return window.JST["tasks.html"];
            },
            events: {
				"click #add-task-btn": "addTask"
            },
            ui: {
                addTaskBtn: "#add-task-btn"
            },
            onRender: function () {
                // Don't need to do anything yet...
            },
            addTask: function(){
                alert("Add Task");
            }
        });

        return Tasks;
    });