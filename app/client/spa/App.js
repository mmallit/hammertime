define([
 "spa/Layout",
 "spa/Menu",
 "spa/Home",
 "spa/SignIn",
 "spa/Tasks"
 ],
    function (Layout, Menu, Home, SignIn, Tasks) {
        var App = Backbone.Marionette.Application.extend({
            init: function () {
                // Create the main layout and region
                this.layout = new Layout();
                this.mainRegion.show(this.layout);

                // allow access to this in inner scope
                var that = this;
                this.token = $.cookie('token');
                // If we have a stored token
                if (this.token) {
                    // Get the user by the token
                    this.getUserByToken(
                        this.token,
                        function (err) {
                            // If we fail show the login
                            console.log(err);
                            that.showLogin();
                        },
                        function (user) {
                            // If we get a user show the main content
                            that.showMainContent(user);
                        }
                    );
                } else {
                    // Show the login screen
                    this.showLogin();
                }
            },
            // Shows the default view (SignIn)
            showLogin: function () {
                if (this.layout.menu.$el) {
                    this.layout.menu.$el.hide();
                }

                this.layout.content.show(new SignIn({
                    app: this
                }));
            },
            // Shows the content view (Dashboard)
            showMainContent: function (user) {
                var that = this;
                // Create the menu UI
                that.layout.menu.show(new Menu({
                    user: user,
                    app: that
                }));
                // If the menu element isn't undefined show it
                if (that.layout.menu.$el) {
                    that.layout.menu.$el.show();
                }
                // Create the main content
                that.layout.content.show(new Home({
                    app: that
                }));
            },
            // Shows the content view (Dashboard)
            showTasks: function () {
                var that = this;
                // Create the main content
                that.layout.content.show(new Tasks({
                    app: that
                }));
            },
            // Activates a user from the registration code 
            // and returns back the user email if successful
            activate: function (key, cbError, cbSuccess) {
                $.ajax({
                    url: "api/activate/" + key,
                    type: "GET",
                    dataType: "json",
                    statusCode: {
                        401: function (data) {
                            cbError(data.responseText);
                        }
                    },
                    success: function (data) {
                        if (data.error) {
                            cbError(data.error);
                        } else {
                            cbSuccess(data);
                        }
                    }
                });
            },
            // Sets the password for the user after activation
            setPassword: function (userId, password, cbError, cbSuccess) {
                var app = this;
                var credentials = {
                    userId: userId,
                    password: password
                };
                $.ajax({
                    url: "api/setPassword",
                    type: "POST",
                    dataType: "json",
                    data: credentials,
                    statusCode: {
                        401: function (data) {
                            cbError(data.responseText);
                        }
                    },
                    success: function (data) {
                        if (data.error) {
                            cbError(data.error);
                        } else {
                            cbSuccess(data);
                        }
                    }
                });
            },
            // Logs in the user with the supplied credentials
            // returns a token for the user
            login: function (username, password, cbError, cbSuccess) {
                var app = this;
                var login = {
                    username: username,
                    password: password
                };
                $.ajax({
                    url: "api/login",
                    type: "POST",
                    dataType: "json",
                    data: login,
                    statusCode: {
                        401: function (data) {
                            cbError(data.responseText);
                        }
                    },
                    success: function (data) {
                        if (data.error) {
                            cbError(data.error);
                        } else {
                            cbSuccess(data);
                        }
                    }
                });
            },
            // Returns back an acccount object from the token
            getUserByToken: function (token, cbError, cbSuccess) {
                $.ajax({
                    url: "api/user/" + token + "?token=" + this.token,
                    type: "GET",
                    dataType: "json",
                    statusCode: {
                        401: function (data) {
                            cbError(data.responseText);
                        },
                        500: function (data) {
                            cbError(data.responseText);
                        }
                    },
                    success: function (data) {
                        if (data.error) {
                            cbError(data.error);
                        } else {
                            cbSuccess(data);
                        }
                    }
                });
            },
            // Ends the user's session by sending the account_id
            endSession: function (id, cbError, cbSuccess) {
                $.ajax({
                    url: "api/user/endSession/" + id + "?token=" + this.token,
                    type: "GET",
                    dataType: "json",
                    statusCode: {
                        401: function (data) {
                            cbError(data.responseText);
                        }
                    },
                    success: function (data) {
                        if (data.error) {
                            cbError(data.error);
                        } else {
                            cbSuccess(data);
                        }
                    }
                });
            },
            // Add a user to the db and have the service send a registration email
            addUser: function (email, name, cbError, cbSuccess) {
                var app = this;
                var user = {
                    email: email,
                    name: name
                };
                $.ajax({
                    url: "api/user/add" + "?token=" + this.token,
                    type: "POST",
                    dataType: "json",
                    data: user,
                    statusCode: {
                        401: function (data) {
                            cbError(data.responseText);
                        }
                    },
                    success: function (data) {
                        if (data.error) {
                            cbError(data.error);
                        } else {
                            cbSuccess(data);
                        }
                    }
                });
            }
        });
        return App;
    });